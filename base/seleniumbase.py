from selenium import webdriver
from selenium.common import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


class SeleniumBase:
    def __init__(self, driver):
        self.driver = driver
        self.__wait = WebDriverWait(driver, 5, 0.5, ignored_exceptions=StaleElementReferenceException)
        self.func = None
    def __get_selenium_by(self, find_by):
        find_by = find_by.lower()
        locating = {'css': By.CSS_SELECTOR,
                    'id': By.ID,
                    'xpath': By.XPATH,
                    'name': By.NAME,
                    'class': By.CLASS_NAME,
                    'tag': By.TAG_NAME,
                    'link': By.LINK_TEXT,
                    'partial_link': By.PARTIAL_LINK_TEXT}
        return locating[find_by]

    def is_visible(self, find_by, locator, locator_name=None):
        return self.__wait.until(ec.visibility_of_element_located((self.__get_selenium_by(find_by), locator)),
                                 locator_name)

    def is_present(self, find_by, locator, locator_name=None):
        return self.__wait.until(ec.presence_of_element_located((self.__get_selenium_by(find_by), locator)), locator_name)

    def is_not_present(self, find_by, locator, locator_name=None):
        return self.__wait.until(ec.invisibility_of_element_located((self.__get_selenium_by(find_by), locator)),
                                 locator_name)

    def are_visible(self, find_by, locator, locator_name=None):
        return self.__wait.until(ec.visibility_of_all_elements_located((self.__get_selenium_by(find_by), locator)),
                                 locator_name)

    def are_presents(self, find_by, locator, locator_name=None):
        return self.__wait.until(ec.presence_of_all_elements_located((self.__get_selenium_by(find_by), locator)),
                                 locator_name)

    def get_text_from_web_elements(self, elements):
        return [element.text for element in elements]


    def get_element_by_text(self, elements, text):
        text = text.lower()
        return [element for element in elements if text == element.text.lower()][0]

    def click(self, func):
        element = func()
        element.click()
