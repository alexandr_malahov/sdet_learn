class Utils:

    @staticmethod
    def join_strings(list_of_strings):
        return ', '.join(list_of_strings)