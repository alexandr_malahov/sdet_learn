import requests


def test_get():
    response = requests.get('https://www.google.com/')
    assert response.status_code == 200
    assert 'Google' in response.text


