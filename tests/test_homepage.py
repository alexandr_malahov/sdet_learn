import pytest
import time

from pom.homepage_nav import HomePageNav


@pytest.mark.usefixtures('setup')
class TestHomePage:

    def test_homapage(self):
        homepage = HomePageNav(self.driver)
        homepage.close_popup()
        actual_links = homepage.get_navlinks_text()
        print(actual_links)
        expected_links = homepage.NAV_LINK_TEXT
        assert expected_links == actual_links, 'wrong links'
        elements = homepage.get_navlinks()
        for element in elements:
            homepage.click(lambda: element)
            time.sleep(2)
            homepage.title()
