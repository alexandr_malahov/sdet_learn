URL = 'https://jsonplaceholder.typicode.com'
ENDPOINTS = {
    'posts': '/posts',
    'comments': '/comments',
    'albums': '/albums',
    'photos': '/photos',
    'todos': '/todos',
    'users': '/users',
    'put': '/put'
}
