from base.seleniumbase import SeleniumBase
from base.utils import Utils


class HomePageNav(SeleniumBase):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver
        self.__nav_links: str = '#mainNavigationFobs>li'
        self.__close_popup: str = '#closeButton'
        self.NAV_LINK_TEXT = 'Women, Men, Kids & Baby, Home, Shoes, Handbags & Accessories, Jewelry, Sale'


    def get_navlinks(self):
        return self.are_visible('css', self.__nav_links, 'Header Navigation links')

    def popup(self):
        return self.is_visible('css', self.__close_popup, 'Close Popup')

    def get_navlinks_text(self):
        nav_links = self.get_navlinks()
        nav_links_text = self.get_text_from_web_elements(nav_links)
        return Utils.join_strings(nav_links_text)

    def get_navlink_by_name(self, text):
        elements = self.get_navlinks()
        return self.get_element_by_text(elements, text)

    def close_popup(self):
        self.click(self.popup)

    def title(self):
        return self.driver.title


